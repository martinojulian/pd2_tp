<h3>Repositorio</h3>
<ul>
    <li>
        https://gitlab.com/martinojulian/pd2_api_libros
    </li>
    <li>
        https://gitlab.com/martinojulian/pd2_api_envios
    </li>
    <li>
        https://gitlab.com/martinojulian/pd2_api_ordenes
    </li>
</ul>

<h3>Pasos basicos para iniciar el proyecto<h3>

>```sh
>$ git clone https://gitlab.com/martinojulian/pd2_api_libros.git
>$ cd pd2_api_libros
>$ git checkout v2.1 
>$ docker-compose up
>```

>```sh
>$ git clone https://gitlab.com/martinojulian/pd2_api_envios.git
>$ cd pd2_api_envios
>$ git checkout v2.1 
>$ docker-compose up
>```

>```sh
>$ git clone https://gitlab.com/martinojulian/pd2_api_ordenes.git
>$ cd pd2_api_ordenes
>$ git checkout v2.1 
>$ docker-compose up
>```

<h5>Deberiamos tener los siguientes contenedores</h5>

>```sh
>$ docker ps
>```
deberia mostrar lo siguiente.
```
CONTAINER ID   IMAGE                         COMMAND                  CREATED          STATUS          PORTS                                       NAMES
7c98f5f6ebd3   pd2_api_envios_envios_api     "python server.py"       16 minutes ago   Up 16 minutes   0.0.0.0:4002->5000/tcp, :::4002->5000/tcp   envios_api
213f9b5b7c53   postgres                      "docker-entrypoint.s…"   16 minutes ago   Up 16 minutes   0.0.0.0:5435->5432/tcp, :::5435->5432/tcp   db_postgres_envios
948712fcf53e   pd2_api_ordenes_ordenes_api   "python server.py"       17 minutes ago   Up 17 minutes   0.0.0.0:4001->5000/tcp, :::4001->5000/tcp   ordenes_api
090b4b7afaa4   postgres                      "docker-entrypoint.s…"   17 minutes ago   Up 17 minutes   0.0.0.0:5434->5432/tcp, :::5434->5432/tcp   db_postgres_ordenes
ff8c735a56c4   pd2_api_libros_libros_api     "python server.py"       51 minutes ago   Up 51 minutes   0.0.0.0:4000->5000/tcp, :::4000->5000/tcp   libros_api
25c01109abc2   postgres                      "docker-entrypoint.s…"   51 minutes ago   Up 51 minutes   0.0.0.0:5433->5432/tcp, :::5433->5432/tcp   db_postgres_libros
```

<h3>Decisiones de diseño</h3>
<h5>Modulacion</h5>
<p>La aplicación se divide en 3 microservicios distintos:</p>

<ul>
    <li>
        Libros: Este microservicio será el encargado de almacenar información de los distintos libros, nos brinda una interfaz para poder agregar, eliminar, modificar y ver los libros disponibles. 
    </li>
    <li>
        Órdenes: Este microservicio será el encargado de crear las órdenes, verificar con es servicio de libros para garantizar su existencia. También  se comunicará con el sistema de envíos para poder generar el envío. 
    </li>
    <li>
        Envíos: Este microservicio se encarga de generar el empaquetado, manejara los vehículos disponibles y generará los envíos.
    </li>
</ul>
<h5>Comunicación</h5>

<p>Se decido que la Comunicación entre estos 3 microservicios se hará de forma síncrona, debido a que tiene una implementación mas simple que la comunicion 
asíncrona.
</p>
<h6>Posibles mejoras</h6>
<p>
Para mejorar la comunicacion entre los distintos microservicios en futuras implementaciones se puede agregar:
<ul>
    <li>
        Cola de mensajeria, como por ejemplo RabbitMQ, para la comunucion entre los microservicios de ordenes y envios. El sistema de ordenes una vez generada la agrega a la cola para que el sistema de envio la pueda procesar y agregar la orden a un envio. En caso de no poder crear un envio, por ej si no hay transporte disponible, se debera encolar nuevamente para ser procesada mas tarde. Con esta mejora logramos que se pueda generar una orden sin depender que se cree el envio, el cual no es necesario que se cree en el momento de generar la orden sino que puede esperar.   
    </li>
    <li>
        Peticiones asincronas al servicio de libros. Tanto el servicio de ordenes como el de envio hacen varias peticiones de forma sincronica para obtener informacion de cada libro, al ser de forma sincronica estamos sumando la latencia de cada peticion de los libros para poder seguir procesando. En este caso se puede mejorar utilizando alguna libreria de python que nos permita hacer peticiones de forma asincronica. Por ejemplo grequests, nos permite realizar varias peticion de forma asincrona y nos devolvera una lista con el response de cada peticion. De esta manera reducimos el tiempo de procesamiento de una orden y envio.
    </li>
</ul>

</p>
<br>
<h5>Comunicación síncrona</h5>

<p>
    Se dicidio utilizar REST y json ya que es un formato legible por lo tanto es mas simple desarrollar y debuggear que aplicacion que estan desarroladas con formatos binarios como por ejemplo protobuf utilizando gRPC, aunque esto ultimos nos propociona una ventajas en rendimiento si es mucho el trafico de la aplicacion.
</p>


<h3>Ejemplo de uso</h3>

<h4>Cargar libros</h4>
<p>
    Hagamos una peticion a la siguiente ruta para cargar datos de editoriales, libros, generos(No tiene rutas para crear)
    <br> 
    http://localhost:4000/cargarDatos (POST)
</p>
<p>
    Verifiquemos que se carguen los datos
    <br>
    http://localhost:4000/autores (GET)
</p>
<img src="./imagenes/autores.png">
<p>
    http://localhost:4000/generos (GET)
</p>
<img src="./imagenes/generos.png">
<p>
    http://localhost:4000/editoriales (GET)
</p>
<img src="./imagenes/editoriales.png">
<br>

<b>Carguemos algunos libros </b>
<p>
http://localhost:4000/libros (POST)
<br>
Ejecutemos con los siquientes 3 body:
<br>
{"nombre": "Rayuela", "volumen": 3,  "autorId": 2,  "generoId": 3,  "editorialId": 1 }
<br>
{"nombre": "Historias de cronopios y de famas", "volumen": 4,  "autorId": 2,  "generoId": 2,  "editorialId": 1 }
<br>
{"nombre": "Love Poems", "volumen": 2,  "autorId": 3,  "generoId": 1,  "editorialId": 2 }
</p>
<p>
Verifiquemos que se hayan cargados los libros: <br>
http://localhost:4000/libros (GET)
</p>
<img src="./imagenes/libros.png">
<br>
<br>
<h5>Generar ordenes</h5>
<p>
Intentemos generar una orden la cual pedimos:
<br>
    <ul>
        <li>
            2 libros "Rayuela"
        </li>
        <li>
            4 libros "Historias de cronopios y de famas"
        </li>
        <li>
            1 libros "Love Poems"
        </li>
    </ul>
Para eso hagamos la siguiente peticion
<br>
http://localhost:4001/ordenes (POST)
<br>
{ "libros": [ {"libroId": 1, "cantidad":2}, {"libroId": 2, "cantidad":4}, {"libroId": 3, "cantidad":1} ] }
</p>
<img src="./imagenes/ordenFallida.png">
<p>
Esta peticion nos falla porque antes de generar una orden debemos tener cargado al menos un vehiculo 
</p>
<br>

<h5>Carguemos vehiculo</h5>
<p>
http://localhost:4002/vehiculos (post)
<br>
{ "patente": "aaa123",  "volumen":200, "horarios": [
        {  "dia": "Lunes", "horaDesde": "10:00",  "horaHasta": "15:30"  },
        {  "dia": "Miercoles",  "horaDesde": "12:00", "horaHasta": "17:00" }
    ]
}
<br>
<b>Verifiquemos que haya cargado el vehiculo</b>
<br>
http://localhost:4002/vehiculos (GET)
</p>
<img src="./imagenes/vehiculos.png">

<br>
<h5>Volvamos a generar orden</h5>
<p>
Para eso hagamos la siguiente peticion
<br>
http://localhost:4001/ordenes (POST)
<br>
{ "libros": [ {"libroId": 1, "cantidad":2}, {"libroId": 2, "cantidad":4}, {"libroId": 3, "cantidad":1} ] }
<br>
Verifiquemos que se haya generado la orden
<br>
http://localhost:4001/ordenes (GET)
</p>
<img src="./imagenes/orden.png">
<p>
Podemos ver que ya se creo la orden, y que tiene asociado el envio
</p>
<br>

<h5>Envios</h5>
<p>
Para ver los envios
<br>
http://localhost:4002/envios (GET)
</p>
<img src="./imagenes/envios.png">
<p>
Podemos ver como se armo el paquete
<br>
http://localhost:4002/paquete/1 (GET)
</p>
<img src="./imagenes/paquete.png">
